package org.wso2.bpmn.tasklistener;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

public class VoteTaskListener implements TaskListener{

	ArrayList<Vote> voteTypes = new ArrayList<Vote>() {{
	    add(new Vote("no", "No",0));
	    add(new Vote("yes", "Yes",0));
	}};
	
	public void notify(DelegateTask arg0) {
		DelegateExecution execution = arg0.getExecution();
		
		String vote = (String)execution.getVariable("vote");		
		Object votes = execution.getVariable("votes");		
		String companions = (String)execution.getVariable("companions");		
		if(votes != null && votes instanceof String ) {
			ArrayList<Vote> opiniaoArrayList = stringToArrayList((String)votes);
			ArrayList<Vote> updatedVotes = findAndUpdateVote(opiniaoArrayList, vote);

			String votesString = arrayListToString(opiniaoArrayList);
			execution.setVariable("votes", votesString);
			
			Integer votesCount = updatedVotes.get(0).getCount() + updatedVotes.get(1).getCount();
			System.out.println("TOTAL COMPANIONS: " + getNumberOfCompanions(companions));
			
			if (votesCount == getNumberOfCompanions(companions)) {
				if (updatedVotes.get(0).getCount() > updatedVotes.get(1).getCount()) { //no
					execution.setVariable("decision", "rejected");
				} else if(updatedVotes.get(0).getCount() < updatedVotes.get(1).getCount()) { //yes
					execution.setVariable("decision", "accepted");
				} else if (updatedVotes.get(0).getCount() == updatedVotes.get(1).getCount()) { //equal
					execution.setVariable("decision", "accepted");
				}
			}
			
		}else {
			String defaultVotes =  arrayListToString(voteTypes);
			
			ArrayList<Vote> voteArrayList = stringToArrayList(defaultVotes);
			ArrayList<Vote> updatedVotes = findAndUpdateVote(voteArrayList, vote);

			String votesString = arrayListToString(voteArrayList);
			execution.setVariable("votes", votesString);

			Integer votesCount = updatedVotes.get(0).getCount() + updatedVotes.get(1).getCount();
			
			if (votesCount == getNumberOfCompanions(companions)) {
				if (updatedVotes.get(0).getCount() > updatedVotes.get(1).getCount()) { //no
					execution.setVariable("decision", "rejected");
				} else if(updatedVotes.get(0).getCount() < updatedVotes.get(1).getCount()) { //yes
					execution.setVariable("decision", "accepted");
				} else if (updatedVotes.get(0).getCount() == updatedVotes.get(1).getCount()) { //equal
					execution.setVariable("decision", "accepted");
				}
			}
			
		}
		
	}
	
	public ArrayList<Vote> findAndUpdateVote(ArrayList<Vote> votes, String id) {
		
		for(Vote vote : votes) {
			if(vote.getId().equals(id)) {
				vote.setCount(vote.getCount() + 1);
				break;
			}
		}
		return votes;
	}
	
	public String arrayListToString(ArrayList<Vote> votes) {
		String votesString = "";
		for(int i =0; i < votes.size();i++) {
			votesString += votes.get(i).toString();
			if(i!=votes.size()-1) {
				votesString += ",";
			}
		}
		return votesString;
	}
	
	public ArrayList<Vote> stringToArrayList(String votes){
		ArrayList<Vote> votesArrayList = new ArrayList<Vote>();
		String[] votesArray = votes.split(",");
		String id, name;
		Integer count;
		for(int i =0; i< votesArray.length;i++) {
			String[] voteProperties = votesArray[i].split("-");
			id = voteProperties[0];
			name = voteProperties[1];
			count = Integer.parseInt(voteProperties[2]);
			System.out.println(id+"-"+name+"-"+count);
			Vote v  = new Vote(id,name,count);
			votesArrayList.add(v);
		}
		return votesArrayList;
	}
	
	
	
	class Vote {
		private String id;
		private String name;
		private Integer count = 0;
		
		public Vote(String id, String name,Integer count) {
			super();
			this.id = id;
			this.name = name;
			this.count = count;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getCount() {
			return count;
		}
		public void setCount(Integer count) {
			this.count = count;
		}
		@Override
		public String toString() {
			return id.trim() + "-" + name.trim() + "-" + count;
		}	
	}
	public Integer getNumberOfCompanions(String companions) {
		String[] companionsArray = companions.split(",");
		return companionsArray.length;
	}
}