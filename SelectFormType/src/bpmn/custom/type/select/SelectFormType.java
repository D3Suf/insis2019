package bpmn.custom.type.select;

import org.activiti.engine.form.AbstractFormType;

public class SelectFormType extends AbstractFormType {
	
	public static final long serialVersionUID = 1L;
	public static final String TYPE_NAME = "selection";

	@Override
	public String getName() {
		return TYPE_NAME;
	}

	@Override
	public Object convertFormValueToModelValue(String arg0) {
		Integer select = Integer.valueOf(arg0);
		return select;
	}

	@Override
	public String convertModelValueToFormValue(Object arg0) {
		if (arg0 == null) {
			return null;
		}
		return arg0.toString();
	}
}
